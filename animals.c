///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Andee Gary <andeeg@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   02_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include "animals.h"

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {
      switch (color)
      {
         case BLACK: return "Black";
         case WHITE: return "White";
         case RED: return "Red";
         case BLUE: return "Blue";
         case GREEN: return "Green";
         case PINK: return "Pink";
      }

   return "Unknown"; // We should never get here
}

