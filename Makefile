###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author Andee Gary <andeeg@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   @todo 02_02_2021
###############################################################################
CC		= gcc
CFLAGS= -g -Wall
TARGET= animalfarm
all: $(TARGET)



main.o: cat.h animals.h
	$(CC) $(CFLAGS) -c main.c

animals.o: cat.h animals.h
	$(CC) $(CFLAGS) -c animals.c

cat.o: cat.h animals.h
	$(CC) $(CFLAGS) -c cat.c

animalfarm: main.o animals.o cat.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o animals.o cat.o

clean:
	rm -f *.o animalfarm
